# UML-project-one

<h1 style="color:blue;">Consignes d'un schéma UML avec Visual Paradigm</h1>

<h2>Modélisation: Association sportive</h2>
<h3>Source: Youtube -> Mickael Desnos</h3>
<hr>
<p>
Le président de l'association sportive de votre commune, vous sachant développeur, vous demande de bien vouloir réaliser le site de votre club. 
Ce site internet devra permettre la gestion de la communication autour de la vie du club, des convocations aux rencontres et des renouvellements de licences principalement. 
</p>
<p>
Le site a vocation à être utilisé par les internautes et les licenciés du club. Ainsi, un adhérent du club viendra sur le site se créer un compte grâce à son numéro de licence afin de passer du statut d'internaute à celui de licencié. En effet, le compte sera activé par la secrétaire si le numéro de licence indiqué dans la demande est connu dans les dossiers papiers du secrétariat et correspond bien à l'état civil du licencié saisi par l'internaute. Le rôle de la secrétaire qui administre les comptes est donc primordial. C'est au moment de cette création de compte que le système demandera au licencié de renseigner ses tailles de maillot, de haut de survêtement, de short et sa pointure.
</p>
<p>
En plus de ce premier service, les internautes pourront, comme tout licencié connu du site, consulter une actualité, les informations du club comme l'organigramme, le projet, l'historique et la charte mais aussi le calendrier et résultats des équipes. Cette dernière consultation se fera via le site de la fédération nationale de rattachement du club. Enfin, le site permettra de renseigner un formulaire en ligne afin de s'adresser au club par mail. 
</p>
<p>
Le licencié aura, de plus la possibilité de consulter les convocations des rencontres des équipes du week-end à venir. En effet, la convocation correspond à une équipe ou plusieurs équipes pour une heure, un jour et un lieu de rendez-vous. Un licencié peut être concerné par plusieurs convocations. Enfin, s'agissant des convocations, l'entraîneur responsable d'équipe devra apparaître à cet endroit du site. Il a été convenu qu'un entraîneur était considéré entraîneur à partir du moment où il s'occupait d'une ou plusieurs équipes. L'entraîneur aura la charge de gérer les convocations de ses équipes. 
</p>
<p>
Le licencié, au moment ou en dehors de la consultation des convocations, devra avoir la possibilité de déclarer une éventuelle indisponibilité. Cette indisponibilité est soit d'une soit de plusieurs journées. La connaissance des dates d'indisponibilités est donc essentielle.
</p>
<p>
Pour la première année d'affiliation au club, la demande de licence est obligatoirement effectuée par courrier à la fédération de rattachement. Les années suivantes, cette procédure postale est complétée par un service de renouvellement en ligne. Le licencié peut, en effet, adresser sa demande par mail à la fédération de rattachement via le site à condition que la transaction bancaire préalable soit acceptée par le système interbancaire distant. Ce service n'est pas offert au primo licencié puisque le renouvellement est contraint par l'indication dans la demande du numéro de la licence.
</p>
<p>
Le licencié pourra aussi consulter les évènements de la vie du club créés au préalable par la secrétaire. Ces évènements sont toutes les activités extra sportives organisées ou qui concernent le club. A chaque création d'un évènement tous les licenciés inscrits sur le site reçoivent un mail d'information
</p>
<p>
Enfin, le licencié pourra commander, via le site, son équipement sportif correspondant aux tailles qu'il aura préalablement renseignées lors de la création de son compte. Il vous est demandé de permettre dans ce cadre la modification des tailles par le licencié. En effet, de manière générale, ce dernier doit pouvoir modifier les informations de son compte dès qu'il le souhaite. Le numéro de licence devra être renseigné lors de cette procédure d'achat validée par le système interbancaire distant. En effet, le stock des équipements étant limité, un seul achat sera accordé par licencié. 
</p>

<h3>
Noms des classes utilisées:
</h3> 
<br>
<ol>
<li>Compte</li>
<li>Licencie</li>
<li>Club</li>
<li>Compte</li>
<li>Compte</li>
<li>Compte</li>
<li>Compte</li>
<li>Compte</li>
<li>Compte</li>
</ol>
